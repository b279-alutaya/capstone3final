import React, { useState } from "react";
import UserContext from "./UserContext";
import AppNavbar from "./AppNavbar";

const UserProvider = ({ children }) => {
  const [user, setUser] = useState({
    firstName: "John",
    lastName: "Doe",
    isAdmin: false,
    id: 1234
  });

  return (
    <UserContext.Provider value={{ user }}>
      {children}
    </UserContext.Provider>
  );
};

export default UserProvider;
