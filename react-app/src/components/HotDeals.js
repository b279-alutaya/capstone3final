import { Row, Col, Card } from "react-bootstrap";

export default function HotDeals(){

	return(
		<Row>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                            <h2>Exclusive Home Delivery Experience</h2>
                        </Card.Title>
                    </Card.Header>
                        <Card.Text>
                        At Home Parcel Delivery made simple! Say goodbye to long waits and inconvenient pickups - with our reliable and efficient service, your parcels will be delivered straight to your doorstep.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                            <h2>Flexible Financing</h2>
                        </Card.Title>
                        </Card.Header>
                        <Card.Text>
                        Shop your favorite products today and enjoy the flexibility of paying later with our convenient Buy Now, Pay Later option. Say goodbye to upfront payments and embrace a hassle-free shopping experience.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                        <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                            <h2>Effortless E-Wallet Checkout</h2>
                        </Card.Title>
                        </Card.Header>
                        <Card.Text>
                        Experience the seamless convenience of Easy Pay with your trusted E-Wallet. Say goodbye to carrying cash or fumbling with cards – now you can make quick and secure payments with just a few taps on your smartphone.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
	)
}
