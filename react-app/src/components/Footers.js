import React from "react";

const Footer =() => {
    return <footer className='footer-bg p-3'>
    <div className="container mx-auto">
        <p className="text-white text-center">Copyright &copy; Empty Wall Inc. 2023 All Rights Reserved
        </p>
    </div>
    </footer>
};

export default Footer;