import React from 'react';
import { Link } from 'react-router-dom';
import bgImage from '../Video/background-img.mp4';
import '../VideoStyle.css';

const Video = () => {
  return (
    <div className="hero">
      <video autoPlay loop muted id="video">
        <source src={bgImage} type="video/mp4" />
      </video>
      <div className="content">
        <h1>Empty Wall Inc.</h1>
        <p>Do No Harm Take No Shit</p>
        <div>
          <Link to="/Products" className="btn btn-light">SHOP NOW</Link>
        </div>
      </div>
    </div>
  );
};

export default Video;
