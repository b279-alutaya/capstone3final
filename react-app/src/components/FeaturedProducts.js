import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function FeaturedProducts() {
    return (
        <div className='d-flex flex-column p-5 text-white bg-white '>
        <h1 className='text-black text-center'>Popular Shirt Picks</h1>
        <Row className="text-dark h-100">
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height" >
                <Card.Img  variant="top"  src={require('../images/shirt1.jpg')} />
                <Card.Header>
                <Card.Title className='text-center'>CC LONGSLEEVE</Card.Title>
                </Card.Header>
                <Card.Footer className='text-center'>
                <Button className="w-50" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src={require('../images/shirt2.jpg')} />
                <Card.Header>
                    <Card.Title className='text-center'>SIGNATURE BLACK</Card.Title>
                    </Card.Header>
                <Card.Footer className='text-center'>
                <Button className="w-50" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src={require('../images/shirt3.jpg')} />
                <Card.Header>
                    <Card.Title className='text-center'>CURVE RED</Card.Title>
                    </Card.Header>
                <Card.Footer className='text-center'>
                <Button className="w-50" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src={require('../images/shirt4.jpg')} />
                <Card.Header>
                    <Card.Title className='text-center'>HANGLOOSE</Card.Title>
                    </Card.Header>
                <Card.Footer className='text-center'>
                <Button className="w-50 gray-button" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src={require('../images/shirt5.jpg')} />
                <Card.Header>
                    <Card.Title className='text-center'>SIGNATURE MAROON</Card.Title>
                    </Card.Header>
                <Card.Footer className='text-center'>
                <Button className="w-50 gray-button" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src={require('../images/shirt6.jpg')} />
                <Card.Header>
                    <Card.Title className='text-center'>DOG FIGHT</Card.Title>
                    </Card.Header>
                <Card.Footer className='text-center'>
                <Button className="w-50 gray-button" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src={require('../images/shirt7.jpg')} />
                <Card.Header>
                    <Card.Title className='text-center'>SHIRT MU</Card.Title>
                    </Card.Header>
                <Card.Footer className='text-center'>
                <Button className="w-50 gray-button" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src={require('../images/shirt8.jpg')} />
                <Card.Header>
                    <Card.Title className='text-center'>ROYAL BLUE</Card.Title>
                    </Card.Header>
                <Card.Footer className='text-center'>
                <Button className="w-50 gray-button" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
        </Row>
        </div>
    );
}
