import { Card, Button, Col } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { FaEye } from 'react-icons/fa';
export default function CourseCard({ props }) {
  const { _id, productName, productBrand, productModel, productType, description, price, stocks, imgSource } = props;

  return (
    <>
      <Col className="my-3" xs={12} md={6} lg={3}>
        <Card className="">
          <div className="position-relative max-h-[390px] group-hover:scale-110 transition duration-300">
            <Card.Img className='img-fluid w-100 product-img-cover ' src={imgSource} />
            <div className="position-absolute top-0 end-0 ">
  <Button className='btn-outline-light' variant='none' as={Link} to={`/products/buy/${_id}`}className='w-12 h-12 bg-white flex justify-center items-center text-primary drop-shadow-xl'>
    <FaEye /> {FaEye}
  </Button>
</div>
          </div>
          <Card.Header className='d-flex align-items-center justify-content-center'>
            <Card.Title>
              {productBrand} {productName} - {productModel}
            </Card.Title>
          </Card.Header>
          <Card.Body>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>Php {price}</Card.Text>
            <Card.Subtitle>Stocks:</Card.Subtitle>
            <Card.Text>
              <p id='stocks'>{stocks} available</p>
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </>
  );
}
