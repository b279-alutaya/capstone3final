import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { FaBars } from "react-icons/fa";
import UserContext from "../UserContext";
import "../NavbarStyle.css";

export default function AppNavbar() {
  const { user } = useContext(UserContext);
  const userFullName = `${user.firstName} ${user.lastName}`;
  console.log(user);

  return (
    <div className="navbar-overlay">
      <Navbar expand="lg" className="main shadow navbar-transparent bg-transparent sticky-top m-0 px-2 text-white">
        <Container fluid>
          <Navbar.Brand as={NavLink} to="/" end className="font-weight-bold text-black">
            <img
              alt=""
              src={require("../images/logo1.jpg")}
              width="30"
              height="30"
              className="d-inline-block align-top rounded"
            />{" "}
            Empty Wall Inc.
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav">
            <FaBars size={20} style={{ color: "#fff" }} />
          </Navbar.Toggle>
          <Navbar.Collapse id="basic-navbar-nav text-white">
            <Nav className="ms-auto">
              {user.isAdmin ? (
                <Nav.Link as={NavLink} to="/dashboard" end>
                  Dashboard
                </Nav.Link>
              ) : (
                <>
                  <Nav.Link as={NavLink} to="/" end>
                    Home
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/products" end>
                    Products
                  </Nav.Link>
                </>
              )}
              {user.id !== null ? (
                <>
                  <NavDropdown title={`Hello ${userFullName}!`} align="end" id="collasible-nav-dropdown" end>
                    <NavDropdown.Item as={NavLink} to="/settings" end>
                      Account Settings
                    </NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/report" end>
                      Report
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item as={NavLink} to="/logout" end>
                      Logout
                    </NavDropdown.Item>
                  </NavDropdown>
                </>
              ) : (
                <>
                  <Nav.Link as={NavLink} to="/login" end>
                    Login
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/register" end>
                    Register
                  </Nav.Link>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
