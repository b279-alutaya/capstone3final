import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { FaBars } from "react-icons/fa";
import UserContext from "../UserContext";
import { Navbar, Nav, NavDropdown, Container } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import "../NavbarStyle.css";

export default function AppNavbar() {
  const { user } = useContext(UserContext);
  const userFullName = `${user.firstName} ${user.lastName}`;
  console.log(user);

  return (
    <div className="header">
      <Link to="/">
        <h1>EMPTY WALL INC</h1>
      </Link>
      <ul className="nav-menu">
        {renderNavLinks(user.isAdmin)}
        {renderUserLinks(user.id, userFullName)}
      </ul>
      <div className="hamburger">
        <FaBars size={20} style={{ color: "#fff" }} />
      </div>
    </div>
  );
}

function renderNavLinks(isAdmin) {
  if (isAdmin) {
    return (
      <>
        <li>
          <Link to="/dashboard">Dashboard</Link>
        </li>
      </>
    );
  } else {
    return (
      <>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/products">Products</Link>
        </li>
      </>
    );
  }
}

function renderUserLinks(userId, userFullName) {
  if (userId !== null) {
    return (
      <>
        <li>
          <Link to="/settings">Account Settings</Link>
        </li>
        <li>
          <Link to="/report">Report</Link>
        </li>
        <li>
          <Link to="/logout">Logout</Link>
        </li>
      </>
    );
  } else {
    return (
      <>
        <li>
          <Link to="/login">Login</Link>
        </li>
        <li>
          <Link to="/register">Register</Link>
        </li>
      </>
    );
  }
}
