import { Navigate } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext";
import HomeCarousel from "../components/HomeCarousel";
import HotDeals from "../components/HotDeals";
import FeaturedProducts from "../components/FeaturedProducts";
import Video from "../components/Video";
import Products from "./Products"
export default function Home(){

	
	const { user } = useContext(UserContext);
	const data = {
		title: "Empty Wall Inc.",
		content: "Your one-stop destination for high-quality fashion that lets you wear your passion with style.",
		destination: "/products",
		label: "Shop Now!",
	}

	return(
		<>
		{
        (user.isAdmin)
		?
		<Navigate to="/dashboard" />
		:
		<div>
		<Video/>
		<Products/>
		</div>
		}
		</>
		
	)
}



