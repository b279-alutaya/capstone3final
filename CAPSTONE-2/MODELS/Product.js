// product.js
const mongoose = require("mongoose");


const productSchema = new mongoose.Schema({
  productModel: {
    type: String,
    required: [true, "Product model is required"],
  },
  productType: {
    type: String,
    required: [true, "Product type is required"],
  },
  productName: {
    type: String,
    required: [true, "Product name is required"],
  },
  description: {
    type: String,
    required: [true, "Product description is required"],
  },
  price: {
    type: Number,
    required: [true, "Product price is required"],
  },
  stocks: {
    type: Number,
    required: [true, "Product stocks is required"],
  },
  imgSource: {
    type: String,
    required: [true, "Product image source is required"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  userOrders: [
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
    },
  ],
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Product", productSchema);
